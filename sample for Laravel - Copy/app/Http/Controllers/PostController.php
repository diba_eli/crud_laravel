<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        //
        //$posts = post::all();
        
        $posts = post::orderBy('id','desc')->paginate(5);
        return view('posts.index',['p'=>$posts]);
    }

    public function create()
    {
        return view('posts.create');
    }

    
    public function store(Request $request)
    {
        //dd($request);
       // return $request->title;
       $this->validate($request, [
            'title'=>'required|min:3',
            'content'=>'required|min:10'

    
       ]);
        //return 'dhaka';
       post::create([
           'title'=>$request->title,
           'content'=>$request->content
       ]);

       return redirect(route('hello.index'));
        
    }

    public function show($post)
    {
        //return $post;
        //dd($post);
         $posts = post::find($post);
         return view('posts.show', ['post'=> $posts]);
        //echo $post->title;
        
        //return view('posts.show', ['post'=> $posts]);
      
    }

    public function edit($post)
    {
        //
        $fetch = post::find($post);
        return view('posts.edit', compact('fetch'));
    }

    public function update(Request $request, $post)
    {
        /*$post->title = $request->title;
        $post->content = $request->content;*/
        //post::where('id',$post)->update($request-all());
        //$post->save();

        
        $p = post::find($post);
       // dd($p);
        $p->title = $request->get('title');
        $p->content = $request->get('content');
        $p->save();
        return redirect(route('hello.index'));

    }

    public function destroy($post)
    {
        $s = post::find($post);
        $s->delete();
        return redirect(route('hello.index'));
    }
}
