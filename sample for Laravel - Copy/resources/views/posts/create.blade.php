@extends('posts.master')

@section('body')
 
    <div class="card mt-2 p-2">
     <h3>Add Post</h3>
     @if($errors->all())
      <div class="alert alert-danger"> 
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
      
      
      </div>

     @endif
     
        <form action ="{{route('hello.store')}}" , method ="post">
        @csrf
          <div class="form-group">
            <label for="comment">title</label>
            <input type="text" class="form-control"  name = "title">
            <label for="comment">content</label>
            <textarea class="form-control" rows="5" name = "content"></textarea>
            <button type = "submit" class="btn btn-outline-primary mt-2">Add</button>
          </div>
        </form>
    </div>
  @endsection