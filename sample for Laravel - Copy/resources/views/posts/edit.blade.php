@extends('posts.master')

@section('body')
 
    <div class="card mt-2 p-2">
     <h3>Add Post</h3>
     @if($errors->all())
      <div class="alert alert-danger"> 
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
      
      
      </div>

     @endif
     
        <form action ="{{route('hello.update',$fetch->id)}}" , method ="post">
        @csrf
        @method('put')
          <div class="form-group">
            <label for="comment">title</label>
            <input type="text" class="form-control"  name = "title" value = "{{$fetch->title}}">
            <label for="comment">content</label>
            <textarea class="form-control" rows="5" name = "content">{{$fetch->content}}</textarea>
            <button type = "submit" class="btn btn-outline-primary mt-2">Update</button>
          </div>
        </form>
    </div>
  @endsection