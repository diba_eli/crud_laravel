
@extends('posts.master')

@section('body')
  @foreach($p as $post)
    <div class="card mt-2 p-2">
     <div class="card-body">
     <a href = "{{route('hello.show', $post->id)}}">{{$post->title}}</a>
    <a href ="{{route('hello.edit', $post->id)}}" class="btn btn-info ml-2">Edit</a>
    <form onsubmit = "return confirm('are you sure you want to delete this post?')" action = "{{route('hello.destroy', $post->id)}}" method ="post" class = "d-inline-block">
    @csrf
    {{method_field('delete')}}
    <button type = "submit" class="btn btn-danger">Delete</button>
    </form>
     </div>
    
    </div>
  @endforeach

  <div class="mt-3">
  {{$p->links()}}</div>
@endsection