@extends('posts.master')

@section('body')
 
    <div class="card-header mt-2">
      <h2>{{$post->title}}</h2>
    </div>
    
    <div class="card-body">
    <h2>{{$post->content}}</h2>
    
    </div>
@endsection